module.exports = {
     "platform": "gitlab",
     "endpoint": "https://gitlab.com/api/v4/",
     "assignees": ["pheidotting"],
     "baseBranches": ["development"],
     "labels": ["renovate"],
     "extends": ["config:recommended"],
     "prConcurrentLimit": 50
   };
